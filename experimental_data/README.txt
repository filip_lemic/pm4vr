R1: 15x15, 1 user, with improvements, random VE
R2: 15x15, 1 user, without improvements, random VE
R3: 10x10, 1 user, with improvements, random VE
R4: 5x5, 1 user, with improvements, random VE
R5: 15x15, 2 users, with improvements, random VE
R6: 15x15, 2 users, without improvements, random VE
R7: 10x10, 2 users, with improvements, random VE
R8: 10x10, 1 user, without improvements, random VE
R9: 5x5, 1 user, without improvements, random VE
R10: 10x10, 2 users, without improvements, random VE
R11: 15x15, 3 users, with improvements, random VE

--------------------------------------------------------
S1: 15x15, 1 user, with improvements, straight VE
S2: 15x15, 1 user, without improvements, straight VE
S3: 10x10, 1 user, with improvements, straight VE
S4: 5x5, 1 user, with improvements, straight VE
S5: 15x15, 2 users, with improvements, straight VE
S6: 15x15, 2 users, without improvements, straight VE
S7: 10x10, 2 users, with improvements, straight VE
S8: 10x10, 1 user, without improvements, straight VE
S9: 5x5, 1 user, without improvements, straight VE
S10: 10x10, 2 users, with improvements, straight VE
S11: 15x15, 3 users, with improvements, straight VE