#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
The goal of this experiment is to use the experimentally collected data in a 15x15 m^2 environment for training LSTM
and GRU-based short-term physical movement trajectory prediction approaches. We distinguish two types of datasets based 
on the virtual experience delivered to the user (i.e., user walking along a straight virtual path and user walking 
along a random virtual path)

Macro-scale metrics: number of redirections per user, average distance between redirections
micro-scale metrics: mean absolute (MAE) and mean squared error (MSE) of 100 ms predictions

"""

__author__ = "Filip Lemic, Thomas Van Onsem, Jakob Struye, Jeroen Famaey, Xavier Costa Perez"
__copyright__ = "Copyright 2021, i2Cat Foundation & Internet Technology and Data Science Lab (IDLab), University of Antwerp - imec"
__version__ = "1.0.0"
__maintainer__ = "Filip Lemic"
__email__ = "filip.lemic@i2cat.net"
__status__ = "Development"


import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
from dataclasses import dataclass
import datetime
from user import User
import prediction
import numpy as np

@dataclass
class Coordinate:
    x: float
    y: float
    z: float


def stringToCoordinate(elem):
    if '(' not in elem:
        return elem
    elem = elem[:-2]
    elem = elem[1:]
    coords = elem.split(',')
    return Coordinate(float(coords[0]), float(coords[1]), float(coords[2]))


def parseLogFile(file):
    if not file:
        return {}
    log_data = {}
    file1 = open(file, 'r')
    lines = file1.readlines()
    i = 0
    times = []
    for line in lines:
        line_stripped = line.replace(' ', '')
        line_stripped = line_stripped.replace('\n', '')
        line_stripped = line_stripped.split('|')

        time = datetime.datetime.strptime(line_stripped[0], '%d-%m-%Y_%H-%M-%S')
        if i == 2:
            times.append(time)
            i = 0
        else:
            i += 1

        data = line_stripped[1].split(':', 1)
        if data[0] not in log_data:
            log_data[data[0]] = []
        log_data[data[0]].append(stringToCoordinate(data[1]))
    log_data['time'] = times
    return log_data


def cleanLogData(data):

	usr1 = User 
	usr1.phy_locations = []
	usr1.virt_locations = []

	for i in range(0, len(data['Receivedrealpos'])):

		# print(data['Receivedrealpos'][i].y)
		User.fill_physical_path(usr1, [data['Receivedrealpos'][i].x, data['Receivedrealpos'][i].y])
		User.fill_virtual_path_from_data(usr1, [data['Receivedvirtpos'][i].x, data['Receivedvirtpos'][i].y])

	return usr1


###########################################################################
print("\nR1 - random virtual experience, 1 user, 15x15 m^2 environment\n")
###########################################################################
r1_data = parseLogFile('../experimental_data/R1.txt')
usr1 = cleanLogData(r1_data)

users = [usr1]


for user in users:

	mse_lstm = prediction.make_and_evaluate_predictions_lstm(User.get_phy_path(user), 9, 1, 2, epoch = 100, batch = 120, layer = 160)
	print("mse_lstm = " + str(mse_lstm))

	mse_lstm_virt = prediction.make_and_evaluate_virtual_predictions_lstm(User.get_phy_and_virt_path(user), 9, 1, 2)
	print("mse_lstm_virt = " + str(mse_lstm_virt))

	mse_gru = prediction.make_and_evaluate_predictions_gru(User.get_phy_path(user), 9, 1, 2, epoch = 100, batch = 120, layer = 160)
	print("mse_gru = " + str(mse_gru))

	mse_gru_virt = prediction.make_and_evaluate_virtual_predictions_gru(User.get_phy_and_virt_path(user), 9, 1, 2, epoch = 100, batch = 120, layer = 160)
	print("mse_gru_virt = " + str(mse_gru_virt))


print("# ++++++++++++++++++++++++++++++++++++++++++")
print()


###########################################################################
print("\nS1 - straight virtual experience, 1 user, 15x15 m^2 environment\n")
###########################################################################
s1_data = parseLogFile('../experimental_data/S1.txt')
usr2 = cleanLogData(s1_data)

users = [usr2]


for user in users:

    mse_lstm = prediction.make_and_evaluate_predictions_lstm(User.get_phy_path(user), 9, 1, 2, epoch = 100, batch = 120, layer = 160)
    print("mse_lstm = " + str(mse_lstm))

    mse_lstm_virt = prediction.make_and_evaluate_virtual_predictions_lstm(User.get_phy_and_virt_path(user), 9, 1, 2, epoch = 100, batch = 120, layer = 160)
    print("mse_lstm_virt = " + str(mse_lstm_virt))

    mse_gru = prediction.make_and_evaluate_predictions_gru(User.get_phy_path(user), 9, 1, 2, epoch = 100, batch = 120, layer = 160)
    print("mse_gru = " + str(mse_gru))

    mse_gru_virt = prediction.make_and_evaluate_virtual_predictions_gru(User.get_phy_and_virt_path(user), 9, 1, 2, epoch = 100, batch = 120, layer = 160)
    print("mse_gru_virt = " + str(mse_gru_virt))


print("# ++++++++++++++++++++++++++++++++++++++++++")
print()



###########################################################################
print("\nR5 - random virtual experience, 2 users, 15x15 m^2 environment\n")
###########################################################################
s1_data = parseLogFile('../experimental_data/S5_U1.txt')
usr3 = cleanLogData(s1_data)
s2_data = parseLogFile('../experimental_data/S5_U2.txt')
usr4 = cleanLogData(s2_data)

users = [usr3, usr4]

i  = 0
for user in users:

    i+=1
    print('User ' + str(i))

    mse_lstm = prediction.make_and_evaluate_predictions_lstm(User.get_phy_path(user), 9, 1, 2, epoch = 100, batch = 120, layer = 160)
    print("mse_lstm = " + str(mse_lstm))

    mse_lstm_virt = prediction.make_and_evaluate_virtual_predictions_lstm(User.get_phy_and_virt_path(user), 9, 1, 2, epoch = 100, batch = 120, layer = 160)
    print("mse_lstm_virt = " + str(mse_lstm_virt))

    mse_gru = prediction.make_and_evaluate_predictions_gru(User.get_phy_path(user), 9, 1, 2, epoch = 100, batch = 120, layer = 160)
    print("mse_gru = " + str(mse_gru))

    mse_gru_virt = prediction.make_and_evaluate_virtual_predictions_gru(User.get_phy_and_virt_path(user), 9, 1, 2, epoch = 100, batch = 120, layer = 160)
    print("mse_gru_virt = " + str(mse_gru_virt))


print("# ++++++++++++++++++++++++++++++++++++++++++")
print()


###########################################################################
print("\nS5 - straight virtual experience, 2 users, 15x15 m^2 environment\n")
###########################################################################
s1_data = parseLogFile('../experimental_data/R5_U1.txt')
usr5 = cleanLogData(s1_data)
s2_data = parseLogFile('../experimental_data/R5_U2.txt')
usr6 = cleanLogData(s2_data)

users = [usr5, usr6]

i  = 0
for user in users:

    i+=1
    print('\nUser ' + str(i))

    mse_lstm = prediction.make_and_evaluate_predictions_lstm(User.get_phy_path(user), 9, 1, 2, epoch = 100, batch = 120, layer = 160)
    print("mse_lstm = " + str(mse_lstm))

    mse_lstm_virt = prediction.make_and_evaluate_virtual_predictions_lstm(User.get_phy_and_virt_path(user), 9, 1, 2, epoch = 100, batch = 120, layer = 160)
    print("mse_lstm_virt = " + str(mse_lstm_virt))

    mse_gru = prediction.make_and_evaluate_predictions_gru(User.get_phy_path(user), 9, 1, 2, epoch = 100, batch = 120, layer = 160)
    print("mse_gru = " + str(mse_gru))

    mse_gru_virt = prediction.make_and_evaluate_virtual_predictions_gru(User.get_phy_and_virt_path(user), 9, 1, 2, epoch = 100, batch = 120, layer = 160)
    print("mse_gru_virt = " + str(mse_gru_virt))


print("# ++++++++++++++++++++++++++++++++++++++++++")
print()


###########################################################################
print("\nR11 - random virtual experience, 3 users, 15x15 m^2 environment\n")
###########################################################################
s1_data = parseLogFile('../experimental_data/R11_U1.txt')
usr7 = cleanLogData(s1_data)
s2_data = parseLogFile('../experimental_data/R11_U2.txt')
usr8 = cleanLogData(s2_data)
s3_data = parseLogFile('../experimental_data/R11_U3.txt')
usr9 = cleanLogData(s3_data)

users = [usr7, usr8, usr9]

i  = 0
for user in users:

    i+=1
    print('User ' + str(i))

    mse_lstm = prediction.make_and_evaluate_predictions_lstm(User.get_phy_path(user), 9, 1, 2, epoch = 100, batch = 120, layer = 160)
    print("mse_lstm = " + str(mse_lstm))

    mse_lstm_virt = prediction.make_and_evaluate_virtual_predictions_lstm(User.get_phy_and_virt_path(user), 9, 1, 2, epoch = 100, batch = 120, layer = 160)
    print("mse_lstm_virt = " + str(mse_lstm_virt))

    mse_gru = prediction.make_and_evaluate_predictions_gru(User.get_phy_path(user), 9, 1, 2, epoch = 100, batch = 120, layer = 160)
    print("mse_gru = " + str(mse_gru))

    mse_gru_virt = prediction.make_and_evaluate_virtual_predictions_gru(User.get_phy_and_virt_path(user), 9, 1, 2, epoch = 100, batch = 120, layer = 160)
    print("mse_gru_virt = " + str(mse_gru_virt))


print("# ++++++++++++++++++++++++++++++++++++++++++")
print()


###########################################################################
print("\nS11 - straight virtual experience, 3 users, 15x15 m^2 environment\n")
###########################################################################
s1_data = parseLogFile('../experimental_data/S11_U1.txt')
usr10 = cleanLogData(s1_data)
s2_data = parseLogFile('../experimental_data/S11_U2.txt')
usr11 = cleanLogData(s2_data)
s3_data = parseLogFile('../experimental_data/S11_U3.txt')
usr12 = cleanLogData(s3_data)

users = [usr10, usr11, usr12]

i  = 0
for user in users:

    i+=1
    print('\nUser ' + str(i))

    mse_lstm = prediction.make_and_evaluate_predictions_lstm(User.get_phy_path(user), 9, 1, 2, epoch = 100, batch = 120, layer = 160)
    print("mse_lstm = " + str(mse_lstm))

    mse_lstm_virt = prediction.make_and_evaluate_virtual_predictions_lstm(User.get_phy_and_virt_path(user), 9, 1, 2, epoch = 100, batch = 120, layer = 160)
    print("mse_lstm_virt = " + str(mse_lstm_virt))

    mse_gru = prediction.make_and_evaluate_predictions_gru(User.get_phy_path(user), 9, 1, 2, epoch = 100, batch = 120, layer = 160)
    print("mse_gru = " + str(mse_gru))

    mse_gru_virt = prediction.make_and_evaluate_virtual_predictions_gru(User.get_phy_and_virt_path(user), 9, 1, 2, epoch = 100, batch = 120, layer = 160)
    print("mse_gru_virt = " + str(mse_gru_virt))


print("# ++++++++++++++++++++++++++++++++++++++++++")
print()
